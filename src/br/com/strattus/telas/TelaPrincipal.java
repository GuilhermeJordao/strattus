/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.strattus.telas;

/**
 *
 * @author Automacao_02
 */
public class TelaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        ContagemTempo = new javax.swing.JLabel();
        jlblTempGraoTexto = new javax.swing.JLabel();
        jlblTempGraoNumero = new javax.swing.JLabel();
        jlblTempGraoCelsius = new javax.swing.JLabel();
        lbldeltaGrao = new javax.swing.JLabel();
        lblDeltaGraoNumero = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        menu_Receitas = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();

        jMenu1.setText("jMenu1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("STRATTUS ");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1169, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 642, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1173, 646));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setText("Descarrega");
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 185, 113, 30));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setText("TORRAR");
        jPanel2.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 10, 113, 30));

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton3.setText("Carregar");
        jPanel2.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 45, 113, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton4.setText("Fim Seca");
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 80, 113, 30));

        jButton5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton5.setText("1° Crack");
        jPanel2.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 115, 113, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton6.setText("2° Crack");
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 150, 113, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Nº Torra");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("___");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 230, -1, -1));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1174, 180, 140, 270));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ContagemTempo.setFont(new java.awt.Font("Tahoma", 0, 32)); // NOI18N
        ContagemTempo.setText("00:00");
        jPanel3.add(ContagemTempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 5, -1, -1));

        jlblTempGraoTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlblTempGraoTexto.setForeground(new java.awt.Color(0, 102, 0));
        jlblTempGraoTexto.setText("Grão");
        jPanel3.add(jlblTempGraoTexto, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 70, -1, -1));

        jlblTempGraoNumero.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jlblTempGraoNumero.setForeground(new java.awt.Color(0, 102, 0));
        jlblTempGraoNumero.setText("---");
        jPanel3.add(jlblTempGraoNumero, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));

        jlblTempGraoCelsius.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlblTempGraoCelsius.setForeground(new java.awt.Color(0, 102, 0));
        jlblTempGraoCelsius.setText("ºC");
        jPanel3.add(jlblTempGraoCelsius, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, -1, -1));

        lbldeltaGrao.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbldeltaGrao.setForeground(new java.awt.Color(0, 0, 153));
        lbldeltaGrao.setText("Delta");
        lbldeltaGrao.setToolTipText("");
        jPanel3.add(lbldeltaGrao, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 130, -1, -1));

        lblDeltaGraoNumero.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        lblDeltaGraoNumero.setForeground(new java.awt.Color(0, 0, 153));
        lblDeltaGraoNumero.setText("0");
        jPanel3.add(lblDeltaGraoNumero, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, -1, -1));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1174, 0, 140, 180));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton7.setText("DESCONECTAR");
        jPanel4.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 113, 30));

        jButton8.setText("CONECTAR");
        jPanel4.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 113, 30));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1174, 450, 140, 90));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/strattus/icones/icone.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 550, 90, 90));

        jMenu2.setText("Cadastros");
        jMenu2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jMenu2.setMaximumSize(new java.awt.Dimension(100, 32767));

        menu_Receitas.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        menu_Receitas.setText("Receitas");
        menu_Receitas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_ReceitasActionPerformed(evt);
            }
        });
        jMenu2.add(menu_Receitas);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Sobre");
        jMenu3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jMenu3.setMaximumSize(new java.awt.Dimension(75, 32767));
        jMenuBar1.add(jMenu3);

        jMenu4.setText("Sair");
        jMenu4.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(1333, 711));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menu_ReceitasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_ReceitasActionPerformed
        // TODO add your handling code here:
         CadastroReceitas receitas = new CadastroReceitas();
         receitas.setVisible(true);
         menu_Receitas.setEnabled(false);
                
    }//GEN-LAST:event_menu_ReceitasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows Classic".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ContagemTempo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel jlblTempGraoCelsius;
    private javax.swing.JLabel jlblTempGraoNumero;
    private javax.swing.JLabel jlblTempGraoTexto;
    private javax.swing.JLabel lblDeltaGraoNumero;
    private javax.swing.JLabel lbldeltaGrao;
    public static javax.swing.JMenuItem menu_Receitas;
    // End of variables declaration//GEN-END:variables
}
